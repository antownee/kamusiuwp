using System;
using Kamusi.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Media.Animation;

namespace Kamusi.Views
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
        }

        //public void MarqueeAnimation()
        //{
        //    double width = pnlNlsContent.ActualWidth - txtMaana.ActualWidth;
        //    pnlNlsContent.Margin = new Thickness(width / 2, 0, 0, 0);
        //    DoubleAnimation doubleAnimation = new DoubleAnimation();
        //    doubleAnimation.From = -txtMaana.ActualHeight;
        //    doubleAnimation.To = pnlNlsContent.ActualHeight;
        //    doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
        //    doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(100));
        //    txtMaana.BeginAnimation(Canvas.BottomProperty, doubleAnimation);
        //}
    }
}
