using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Kamusi.Views
{
    public sealed partial class Yaliyomo : Page
    {
        Template10.Services.SerializationService.ISerializationService _SerializationService;

        public static int pageIndex;

        public Yaliyomo()
        {
            InitializeComponent();
            _SerializationService = Template10.Services.SerializationService.SerializationService.Json;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var index = int.Parse(_SerializationService.Deserialize(e.Parameter?.ToString()).ToString());
            MyPivot.SelectedIndex = index;
            pageIndex = index;
        }
    }
}
