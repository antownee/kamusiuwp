﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Kamusi.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MethaliSearch : Page
    {
        public static VisualState NarrowVisualState { get; set; } = null;
        public static VisualState CurrentVisualState { get; set; }

        public MethaliSearch()
        {
            this.InitializeComponent();
            NarrowVisualState = VisualStateNarrow;
            CurrentVisualState = AdaptiveVisualStateGroup.CurrentState;
        }

        private void AdaptiveVisualStateGroup_CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            CurrentVisualState = e.NewState;
        }

        private void lstSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Hide searchGrid
            AdaptiveChange(CurrentVisualState);
        }

        public void AdaptiveChange(VisualState vState)
        {
            //if vstate == narrow, navigate to content page
            if (vState == NarrowVisualState)
            {
                //Hide list, show contentcolumn
                if (CurrentVisualState == NarrowVisualState)
                {
                    searchColumn.Width = new GridLength(0);
                    contentColumn.Width = new GridLength(1, GridUnitType.Star);
                }
            }
        }
    }
}
