﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kamusi.Models
{
    public class SearchData
    {
        public SearchData()
        {
            loadNenoList();
            loadMethaliList();
        }

        public static List<NenoModel> loadNenoList()
        {
            List<NenoModel> lstNeno = new List<NenoModel>();
            lstNeno.Add(new NenoModel { Neno = "a!" });
            lstNeno.Add(new NenoModel { Neno = "aalam" });
            lstNeno.Add(new NenoModel { Neno = "aali" });
            lstNeno.Add(new NenoModel { Neno = "aalimu" });
            lstNeno.Add(new NenoModel { Neno = "aathari" });
            lstNeno.Add(new NenoModel { Neno = "aazi" });
            lstNeno.Add(new NenoModel { Neno = "abaa" });
            lstNeno.Add(new NenoModel { Neno = "abadani" });
            lstNeno.Add(new NenoModel { Neno = "abadi" });
            lstNeno.Add(new NenoModel { Neno = "abakusi" });
            lstNeno.Add(new NenoModel { Neno = "abdi" });
            lstNeno.Add(new NenoModel { Neno = "abedari" });
            lstNeno.Add(new NenoModel { Neno = "abee" });
            lstNeno.Add(new NenoModel { Neno = "abidi" });
            lstNeno.Add(new NenoModel { Neno = "abiri" });
            lstNeno.Add(new NenoModel { Neno = "abiria", Maana = "Mtu anayetoka mahali fulani na kwenda pengine kwa kutumia chombo kwa mfano basi,treni na kadhalika na ambaye si mfanyakazi wa chobo hicho."});
            lstNeno.Add(new NenoModel { Neno = "abjadi" });
            lstNeno.Add(new NenoModel { Neno = "abra" });
            lstNeno.Add(new NenoModel { Neno = "abtadi" });
            lstNeno.Add(new NenoModel { Neno = "abtali" });

            return lstNeno;
        }

        public static List<MethaliModel> loadMethaliList()
        {
            List<MethaliModel> lstMeth = new List<MethaliModel>();
            lstMeth.Add(new MethaliModel { Methali = "Adhabu ya kaburi aijua maiti" });
            lstMeth.Add(new MethaliModel { Methali = "Akiba haiozi" });
            lstMeth.Add(new MethaliModel { Methali = "Asifuye mvuwa imemnyea" });
            lstMeth.Add(new MethaliModel { Methali = "Akili nyingi huondowa maarifa" });
            lstMeth.Add(new MethaliModel { Methali = "Asiye kubali kushindwa si mshindani" });
            lstMeth.Add(new MethaliModel { Methali = "Atangaye na jua hujuwa" });
            lstMeth.Add(new MethaliModel { Methali = "Asiye kuwapo na lake halipo" });
            lstMeth.Add(new MethaliModel { Methali = "Avumaye baharini papa kumbe wengi wapo" });
            lstMeth.Add(new MethaliModel { Methali = "Baada ya dhiki faraja" });
            lstMeth.Add(new MethaliModel { Methali = "Baniani mbaya kiatu chake dawa" });
            lstMeth.Add(new MethaliModel { Methali = "Bendera hufuata upepo" });

            return lstMeth;
        }
    }
}
