﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kamusi.Models
{
    public class Penda
    {
        private NenoModel _neno;

        public NenoModel Neno
        {
            get { return _neno; }
            set { _neno = value; }
        }

        private bool _isPenda;

        public bool IsPenda
        {
            get { return _isPenda; }
            set { _isPenda = value; }
        }

    }
}
