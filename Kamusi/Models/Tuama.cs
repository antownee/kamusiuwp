﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kamusi.Models
{
    public class Tuama
    {
        private NenoModel _neno;

        public NenoModel Neno
        {
            get { return _neno; }
            set { _neno = value; }
        }

        private bool _isTuama;

        public bool IsTuama
        {
            get { return _isTuama; }
            set { _isTuama = value; }
        }

        private List<Tuama> _lstTuama;

        public List<Tuama> lstTuama
        {
            get { return _lstTuama; }
            set { _lstTuama = value; }
        }


    }
}
