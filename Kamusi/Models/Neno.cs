﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kamusi.Models
{
    public class NenoModel
    {
        private string _neno;

        public string Neno
        {
            get { return _neno; }
            set { _neno = value; }
        }

        private string _maana;

        public string Maana
        {
            get { return _maana; }
            set { _maana = value; }
        }

    }
}
