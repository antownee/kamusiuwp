﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Template10.Common;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using Kamusi.Models;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Kamusi.Views;

namespace Kamusi.ViewModels
{
    public class MethaliSearchViewModel : ViewModelBase
    {
        public List<MethaliModel> lstMethali { get; set; }

        public MethaliSearchViewModel()
        {
            lstMethali = new List<MethaliModel>();
            lstMethali = SearchData.loadMethaliList();
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
            }
        }

        private MethaliModel _selectedMethali;

        public MethaliModel SelectedMethali
        {
            get { return _selectedMethali; }
            set
            {
                Set(ref _selectedMethali, value);
            }
        }

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            var sel = (suspensionState.ContainsKey(nameof(SelectedMethali))) ? suspensionState[nameof(SelectedMethali)] : (parameter as NenoModel);
            SelectedMethali = sel as MethaliModel;
            await Task.CompletedTask;
        }

        public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
        {
            if (suspending)
            {
                suspensionState[nameof(SelectedMethali)] = SelectedMethali;
            }
            await Task.CompletedTask;
        }

        public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
        {
            args.Cancel = false;
            await Task.CompletedTask;
        }
        
        public IEnumerable<MethaliModel> GetMatchingMethali(string query)
        {            
            return lstMethali
                .Where(c => c.Methali.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)
                .OrderByDescending(c => c.Methali.StartsWith(query, StringComparison.CurrentCultureIgnoreCase));
        }

        public void TextChanged(object sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            //If the input was typed
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                var ab = sender as AutoSuggestBox;
                ab.ItemsSource = GetMatchingMethali(ab.Text);
            }
        }

        //On press enter
        public void QuerySubmitted(object sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                SelectedMethali = args.ChosenSuggestion as MethaliModel;
            }
            else
            {
                var meths = GetMatchingMethali(args.QueryText);
                if (meths.Count() >= 1)
                {
                    SelectedMethali = meths.FirstOrDefault();
                }
                else
                {
                    //No results
                }
            }
        }

        public void SuggestionChosen(object sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            SelectedMethali = args.SelectedItem as MethaliModel;
            var ab = (sender as AutoSuggestBox).Text = "";
        }
        
    }
}
