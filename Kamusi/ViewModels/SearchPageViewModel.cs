﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Template10.Common;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using Kamusi.Models;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Kamusi.Views;

namespace Kamusi.ViewModels
{
    public class SearchPageViewModel : ViewModelBase
    {
        public List<NenoModel> lstManeno { get; set; }

        public static NenoModel selneno { get; set; }

        public SearchPageViewModel()
        {
            lstManeno = new List<NenoModel>();
            lstManeno = SearchData.loadNenoList();
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
            }
        }

        private NenoModel _selectedNeno;

        public NenoModel SelectedNeno
        {
            get { return _selectedNeno; }
            set
            {
                Set(ref _selectedNeno, value);
            }
        }

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            var sel = (suspensionState.ContainsKey(nameof(SelectedNeno))) ? suspensionState[nameof(SelectedNeno)] : (parameter as NenoModel);
            SelectedNeno = sel as NenoModel;
            await Task.CompletedTask;
        }

        public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
        {
            if (suspending)
            {
                suspensionState[nameof(SelectedNeno)] = SelectedNeno;
            }
            await Task.CompletedTask;
        }

        public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
        {
            args.Cancel = false;
            await Task.CompletedTask;
        }
        
        public IEnumerable<NenoModel> GetMatchingContacts(string query)
        {            
            return lstManeno
                .Where(c => c.Neno.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)
                .OrderByDescending(c => c.Neno.StartsWith(query, StringComparison.CurrentCultureIgnoreCase));
        }

        public void TextChanged(object sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            //If the input was typed
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                var ab = sender as AutoSuggestBox;
                ab.ItemsSource = GetMatchingContacts(ab.Text);
            }
        }

        //On press enter
        public void QuerySubmitted(object sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                SelectedNeno = args.ChosenSuggestion as NenoModel;
            }
            else
            {
                var nenos = GetMatchingContacts(args.QueryText);
                if (nenos.Count() >= 1)
                {
                    SelectedNeno = nenos.FirstOrDefault();
                }
                else
                {
                    //No results
                }
            }
        }

        public void SuggestionChosen(object sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            SelectedNeno = args.SelectedItem as NenoModel;
            var ab = (sender as AutoSuggestBox).Text = "";
        }
        

    }
}
