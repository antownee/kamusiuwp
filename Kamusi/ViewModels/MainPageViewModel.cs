using Template10.Mvvm;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Windows.UI.Xaml.Navigation;
using Kamusi.Models;
using Windows.UI.Xaml.Media.Animation;

namespace Kamusi.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel()
        {
            NenolaSiku = new NenoModel()
            {
                Neno = "abiria",
                Maana = "Mtu anayetoka mahali fulani na kwenda pengine kwa kutumia chombo kwa mfano basi, treni na kadhalika na ambaye si mfanyakazi wa chombo hicho."
            };
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
            }
        }

        private NenoModel _nenolaSiku;

        public NenoModel NenolaSiku
        {
            get { return _nenolaSiku; }
            set
            {
                Set(ref _nenolaSiku, value);
            }
        }


        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            if (suspensionState.Any())
            {
                NenolaSiku = suspensionState[nameof(NenolaSiku)] as NenoModel;
            }
            await Task.CompletedTask;
        }

        public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
        {
            if (suspending)
            {
                suspensionState[nameof(NenolaSiku)] = NenolaSiku;
            }
            await Task.CompletedTask;
        }

        public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
        {
            args.Cancel = false;
            await Task.CompletedTask;
        }

        public void GotoDetailsPage() =>
            NavigationService.Navigate(typeof(Views.DetailPage), NenolaSiku);

        public void GotoSettings() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 0);

        public void GotoPrivacy() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 1);

        public void GotoAbout() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 2);

        public void GotoManeno() =>
            NavigationService.Navigate(typeof(Views.Search),null, new SuppressNavigationTransitionInfo());

        public void GotoMethali() => 
            NavigationService.Navigate(typeof(Views.MethaliSearch),null, new SuppressNavigationTransitionInfo());

        public void GoToNenoLaSiku() =>
            NavigationService.Navigate(typeof(Views.NenoContent),NenolaSiku, new SuppressNavigationTransitionInfo());

    }
}

