﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Template10.Common;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using Kamusi.Models;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;

namespace Kamusi.ViewModels
{
    public class NenoContentViewModel : ViewModelBase
    {

        public NenoContentViewModel()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
            }
        }

        private NenoModel _selectedNeno;

        public NenoModel SelectedNeno
        {
            get { return _selectedNeno; }
            set
            {
                Set(ref _selectedNeno, value);
            }
        }


        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            var sel = (suspensionState.ContainsKey(nameof(SelectedNeno))) ? suspensionState[nameof(SelectedNeno)] : (parameter as NenoModel);
            SelectedNeno = sel as NenoModel; 
            await Task.CompletedTask;
        }

        public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
        {
            if (suspending)
            {
                suspensionState[nameof(SelectedNeno)] = SelectedNeno;
            }
            await Task.CompletedTask;
        }

        public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
        {
            args.Cancel = false;
            await Task.CompletedTask;
        }
        
        
    }
}
